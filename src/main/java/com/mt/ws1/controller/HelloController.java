package com.mt.ws1.controller;

import com.mt.ws1.model.Cat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @RequestMapping("/")
    public @ResponseBody Cat hello(){
        Cat cat = new Cat();
        cat.setId("1");
        cat.setName("Bigodes");
        cat.setBreed("Vira Lata");
        return cat;
    }
}
